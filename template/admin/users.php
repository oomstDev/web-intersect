<div class="pure-g marge">
    <div class="pure-u-1-8"></div>
    <div class="pure-u-3-4">
        <div class="panel">
            <div class="description">
                <a href="<?= App::asset('admin/index')?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?></a>
            </div>
        </div>
        <div class="panel white">
            <div class="panel">
                <div class="title black">
                    <?= Language::getWord('List of account', 'admin', 'users', 'name') ?>
                </div>
                <div class="description white">
                <?php
                if(Router::get('page', 0) > 0){
                    ?>
                    <a href="<?= App::asset('admin/users')?>?page=<?= (Router::get('page')-1).""?>" class="pure-button button-secondary">< <?= Language::getWord('Previous', 'admin', 'previous') ?></a>
                    <?php
                }elseif(count(App::getVar('users')) == 25){
                    ?>
                    <div class="text-right">
                        <a href="<?= App::asset('admin/users')?>?page=<?= (Router::get('page', 0)+1).""?>" class="pure-button button-secondary"><?= Language::getWord('Next', 'admin', 'next') ?> ></a>
                    </div>
                    <?php
                }
                ?>
                <table class="pure-table pure-table-horizontal">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                        <th><?= Language::getWord('Email', 'global', 'email') ?></th>
                        <th><?= Language::getWord('Last Connected', 'user', 'lastConnected') ?></th>
                        <th><?= Language::getWord('Character', 'admin', 'maps', 'character') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var User $user */
                    foreach(App::getVar('users') as $user){
                        ?>
                        <tr>
                            <td><?= $user->getId() ?></td>
                            <td><?= $user->getName() ?></td>
                            <td><?= $user->getMail() ?></td>
                            <td>-</td>
                            <td>
                                <?php
                                /** @var Character $character */
                                foreach ($user->getCharacter() as $character){
                                    ?>
                                    - <?= $character->getName() ?> (<?= $character->getLevel() ?>)
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>