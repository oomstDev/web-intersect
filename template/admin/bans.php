<div class="pure-g marge">
    <div class="pure-u-1-8"></div>
    <div class="pure-u-3-4">
        <div class="panel">
            <div class="description">
                <a href="<?= App::asset('admin/index')?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'admin', 'return') ?></a>
            </div>
        </div>
        <div class="panel">
            <div class="title black">
                <?= Language::getWord('List of banned accounts', 'admin', 'bans', 'name') ?>
            </div>
            <div class="description white">
                <?php
                if(Router::get('page', 0) > 0){
                    ?>
                    <a href="<?= App::asset('admin/bans')?>?page=<?= (Router::get('page')-1).""?>" class="pure-button button-secondary">< <?= Language::getWord('Previous', 'admin', 'previous') ?></a>
                    <?php
                }elseif(count(App::getVar('bans')) == 25){
                    ?>
                    <div class="text-right">
                        <a href="<?= App::asset('admin/bans')?>?page=<?= (Router::get('page', 0)+1).""?>" class="pure-button button-secondary"><?= Language::getWord('Next', 'admin', 'next') ?> ></a>
                    </div>
                    <?php
                }
                ?>
                <table class="pure-table pure-table-horizontal">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Language::getWord('IP', 'admin', 'bans', 'ip') ?></th>
                        <th><?= Language::getWord('ID Account', 'admin', 'bans', 'idAccount') ?></th>
                        <th><?= Language::getWord('Start Account', 'admin', 'bans', 'start') ?></th>
                        <th><?= Language::getWord('End Account', 'admin', 'bans', 'end') ?></th>
                        <th><?= Language::getWord('Reason', 'admin', 'bans', 'reason') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var Ban $ban */
                    foreach(App::getVar('bans') as $ban){
                        ?>
                        <tr>
                            <td><?= $ban->getId() ?></td>
                            <td><?= $ban->getIp() ?></td>
                            <td><?= $ban->getPlayerID() ?></td>
                            <td><?= $ban->getStartTime() ?></td>
                            <td><?= $ban->getEndTime() ?></td>
                            <td><?= $ban->getReason() ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>