<?php
//CHECK ALL EXTENSION
$moduleArray = ['zip', 'PDO', 'pdo_mysql', 'pdo_sqlite', 'curl', 'openssl'];
foreach($moduleArray as $module){
    if(!extension_loaded($module)) die('[ERROR] Need enable module '.$module.' (in php.ini)');
}
$gitInstall = "https://gitlab.com/oomstDev/web-intersect/raw/master/";

function saveConfig($json, $file){
    if(file_exists($file)){
        $fp = fopen($file, "r+");
        ftruncate($fp, 0);
        fclose($fp);
    }
    file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
}

function save($c, $w, $typeInstall, $install){
    global $gitInstall;
    $config = json_decode(file_get_contents($gitInstall.'new_config.json'), true);

    if(!empty($c['name'])) $config['server']['id_0']['name'] = $c['name'];
    if(!empty($c['url'])) $config['server']['id_0']['intersect_folder_server'] = $c['url'];
    $c['directory'] .= (substr($c['directory'],-1) != "/") ? '/' : '';
    if(!empty($c['directory']) && $typeInstall == "local") $config['server']['id_0']['intersect_folder_server'] = $c['directory'];

    if($typeInstall == "remote"){
        $config['servers']['id_0']['config_server'] = array(
            'PlayerDatabase' => array(
                "Type" => "mysql",
                "Server" => $c['playerDatabase']['url'],
                "Port" => $c['playerDatabase']['port'],
                "Database" => $c['playerDatabase']['database'],
                "Username" => $c['playerDatabase']['username'],
                "Password" => $c['playerDatabase']['password']
            ),
            'GameDatabase' => array(
                "Type" => "mysql",
                "Server" => $c['gameDatabase']['url'],
                "Port" => $c['gameDatabase']['port'],
                "Database" => $c['gameDatabase']['database'],
                "Username" => $c['gameDatabase']['username'],
                "Password" => $c['gameDatabase']['password']
            )
        );
    }else{
        file_put_contents("runServer.exe", file_get_contents($gitInstall."install/runServer.exe"));
    }
    if(!empty($w['name'])) $config['website']['title'] = $w['name'];

    $w['path'] = (substr($w['path'],0,1) != "/" ? '/' : '').$w['path'];
    if(!empty($w['path'])) $config['website']['path'] = $w['path'];

    if($install == "02"){
        $exec = file_get_contents($gitInstall."install/Intersect_Server.exe");
        if(!is_dir($c['directory']))mkdir($c['directory'], 0777, true);
        file_put_contents($c['directory']."Intersect Server.exe", $exec);
        $tempFolder = substr($c['directory'], 0, -1);
        $console = popen('"runServer.exe" "'.__DIR__.'\\'.$tempFolder.'" "Intersect Server.exe"', 'r');
        sleep(10);
        pclose($console);
    }
    $dataZip = file_get_contents($gitInstall."install/Intersect_Panel.zip");
    file_put_contents('install.zip', $dataZip);
    $zip = new ZipArchive;
    $zip->open('install.zip');
    $zip->extractTo('./');
    $zip->close();
    unlink('install.zip');

    saveConfig($config, "config.json");
    return true;
}

if(!empty($_POST['install'])){
    set_time_limit(120);
    $install = $_POST['install'];
    $typeInstall = ($install == "04")? "remote" : "local";
    $c = $_POST['config'][$typeInstall];
    $w = $_POST['config']['panel'];

    $v = save($c, $w, $typeInstall, $install);
    if($v){
        header("location: index");
        exit();
    }
}
?>
<html lang="en-us">
    <head>
        <title>Install Intersect Panel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css" rel="stylesheet" type="text/css" />
        <link href="http://www.drastal-online.fr/nico/css/pure.css" type="text/css" rel="stylesheet">
        <link href="http://www.drastal-online.fr/nico/css/design.css" type="text/css" rel="stylesheet">
        <script
            src="https://code.jquery.com/jquery-3.4.0.slim.min.js"
            integrity="sha256-ZaXnYkHGqIhqTbJ6MB4l9Frs/r7U4jlx7ir8PJYBqbI="
            crossorigin="anonymous"></script>
    </head>
    <body>
        <form action="gui.php" class="pure-form pure-form-stacked" method="post">
            <input name="install" type="hidden" value="00" />
            <div class="container">
                <div class="pure-g">
                    <div class="pure-u-1-4"></div>
                    <div class="pure-u-1-2">
                        <div class="panel">
                            <div class="title black text-center">
                                Install Intersect Panel
                            </div>
                            <div class="description white 01">
                                Welcome to install Panel Intersect<br>
                                Choice option:<br>
                                [0] - Create new server Intersect with Panel [not work in web hosting]<br>
                                [1] - Connect local server Intersect to Panel [not work in web hosting]<br>
                                [2] - Connect remote server Intersect to Panel<br><br>
                                <label for="mode">Option:</label>
                                <select onchange="selectMode()" id="mode">
                                    <option disabled selected>-</option>
                                    <option value="02">Create new server Intersect + panel</option>
                                    <option value="03">Connect local server</option>
                                    <option value="04">Connect remote server</option>
                                </select>
                            </div>
                            <div class="description white 02 03">
                                <label for="gameName">Game name:</label>
                                <input id="gameName" type="text" placeholder="Name Intersect Game" name="config[local][name]">
                                <label for="urlServer">Url server:</label>
                                <input id="urlServer" type="text" placeholder="http://example.com" name="config[local][url]">
                                <label for="directoryServer">Directory server:</label>
                                <input id="directoryServer" type="text" placeholder="server" name="config[local][directory]">
                                <br>
                                <a class="pure-button button-success" onclick="descriptionNext('01')">< Previous</a>
                                <a class="pure-button button-success" onclick="descriptionNext('05')">Next ></a>
                            </div>
                            <div class="description white 04">
                                <label for="gameName">Game name:</label>
                                <input id="gameName" type="text" placeholder="Name Intersect Game" name="config[remote][gameName]">
                                <label for="urlServer">Url server:</label>
                                <input id="urlServer" type="text" placeholder="http://example.com" name="config[remote][urlServer]">
                                <br>
                                WARNING! Remote Server is available only MySQL !<br>
                                <b>Server Player Database:</b>
                                <label for="urlServerPlayer">Url:</label>
                                <input id="urlServerPlayer" type="text" placeholder="http://example.com" name="config[remote][playerDatabase][url]">
                                <label for="portServerPlayer">Port:</label>
                                <input id="portServerPlayer" type="number" placeholder="3306" name="config[remote][playerDatabase][port]">
                                <label for="databasePlayer">Database:</label>
                                <input id="databasePlayer" type="text" placeholder="name" name="config[remote][playerDatabase][database]">
                                <label for="userPlayer">Username:</label>
                                <input id="userPlayer" type="text" placeholder="root" name="config[remote][playerDatabase][username]">
                                <label for="passPlayer">Password:</label>
                                <input id="passPlayer" type="password" placeholder="password" name="config[remote][playerDatabase][password]">
                                <a class="pure-button button-secondary" onclick="sameConfig()">Same configuration for Game Database</a>
                                <br>
                                <b>Server Game Database</b>
                                <label for="urlServerGame">Url:</label>
                                <input id="urlServerGame" type="text" placeholder="http://example.com" name="config[remote][gameDatabase][url]">
                                <label for="portServerGame">Port:</label>
                                <input id="portServerGame" type="number" placeholder="3306" name="config[remote][gameDatabase][port]">
                                <label for="databaseGame">Database:</label>
                                <input id="databaseGame" type="text" placeholder="name" name="config[remote][gameDatabase][database]">
                                <label for="userGame">Username:</label>
                                <input id="userGame" type="text" placeholder="root" name="config[remote][gameDatabase][username]">
                                <label for="passGame">Password:</label>
                                <input id="passGame" type="password" placeholder="password" name="config[remote][gameDatabase][password]">
                                <br>
                                <a class="pure-button button-success" onclick="descriptionNext('01')">< Previous</a>
                                <a class="pure-button button-success" onclick="descriptionNext('05')">Next ></a>
                            </div>
                            <div class="description white 05">
                                <label for="panelName">Panel Name:</label>
                                <input id="panelName" type="text" placeholder="Name Intersect Panel" name="config[panel][name]">
                                <label for="urlServer">Path:</label>
                                <input id="urlServer" type="text" placeholder="/panel" name="config[panel][path]">
                                <a class="pure-button button-success" onclick="descriptionNext('01')">< Previous</a>
                                <button type="submit" class="pure-button button-success">Install</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script>

            $(document).ready(function(){
                $('.description').css('display', 'none').css('text-align', 'left');
                $('.01').show();
            });

            function selectMode(){
                let el = $('#mode').val();
                $("[name=install]").val(el);
                descriptionNext(el);
            }
            function descriptionNext(el){
                $('.description').css('display', 'none');
                $('.'+el).show();
            }
            function sameConfig(){
                $('#urlServerGame').val($('#urlServerPlayer').val());
                $('#portServerGame').val($('#portServerPlayer').val());
                $('#databaseGame').val($('#databasePlayer').val());
                $('#userGame').val($('#userPlayer').val());
                $('#passGame').val($('#passPlayer').val());
            }
        </script>
    </body>
</html>
