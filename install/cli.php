<?php
(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && die('cli only');
//CHECK ALL EXTENSION
echo "Welcome to install Panel Intersect\r\n";
$moduleArray = ['zip', 'PDO', 'pdo_mysql', 'pdo_sqlite', 'curl', 'openssl'];
foreach($moduleArray as $module){
    if(extension_loaded($module)) echo '[SUCCESSFUL] '.$module." is loaded\r\n";
    else die('[ERROR] Need enable module '.$module.' (in php.ini)');
}

//DEFINE DEFAULT VARIABLE
$handle = fopen ("php://stdin","r");

$firstStep = -1;
$nameGame = "Game Server"; $linkGame = "localhost"; $folderGame = "";
$titlePanel = "Intersect Panel"; $pathPanel = "/";
$gitInstall = "https://gitlab.com/The-Nico26/web-intersect/raw/U0_9/";

$config = json_decode(file_get_contents($gitInstall.'new_config.json'), true);
$config['servers']['id_0'] = array(
    "name"=>"Game Intersect",
    "intersect_folder_server" => "",
    "intersect_ip" => "localhost",
    "ranking"=>array(
        "0"=>array(
            "type"=>"level"
        ),
        "1"=>array(
            "type"=>"exp"
        )
    ),
    "config_server"=>array()
);
do {
    ?>

Choice option:
[0] - Create new server Intersect with Panel [not work in web hosting]
[1] - Connect local server Intersect to Panel [not work in web hosting]
[2] - Connect remote server Intersect to Panel
[3] - Exit program
=><?php $line = trim(fgets($handle));
    switch ($line){
        case '0':
            newServerIntersect();
            $firstStep = 0;
            break;
        case '1':
            localServerIntersect();
            $firstStep = 1;
            break;
        case '2':
            $firstStep = 2;
            break;
        case '3':
            echo "Bye\t\n";
            exit();
            break;
        default:
            $firstStep = -1;
            break;
    }
}while($firstStep == -1);

echo "\t\nYour link panel is: http://". gethostbyname(gethostname()).$pathPanel;


function baseConfig($folder = true){
    global $handle, $nameGame, $linkGame, $folderGame, $config, $gitInstall;
    echo 'Game name ['.$nameGame.']:';
    if(($value = trim(fgets($handle))) != "") $nameGame = $value;

    echo 'IP/Hostname server Intersect ['.$linkGame.']:';
    if(($value = trim(fgets($handle))) != "") $linkGame = $value;

    if($folder){
        echo 'Folder server Intersect ['.$folderGame.']:';
        if(($value = trim(fgets($handle))) != "") $folderGame = $value;
        if(substr($folderGame, -1) != '/') $folderGame .= '/';
        file_put_contents("runServer.exe", file_get_contents($gitInstall."install/runServer.exe"));

        $config['servers']["id_0"]['intersect_folder_server'] = $folderGame;
    }

    $config['servers']["id_0"]['name'] = $nameGame;
    $config['servers']["id_0"]['intersect_ip'] = $linkGame;
}

function newServerIntersect(){
    global $folderGame, $gitInstall;
    $folderGame = './';
    baseConfig();
    echo "Download Server Intersect... Waiting...\t\n\t\n";
    $exec = file_get_contents($gitInstall."install/Intersect_Server.exe");
    if(!is_dir($folderGame))mkdir($folderGame, 0777, true);
    file_put_contents($folderGame."Intersect Server.exe", $exec);

    echo "[SUCCESS] Download done ! Execute server\t\n";
    $tempFolder = substr($folderGame, 0, -1);
    echo "Exec: \"test.exe\" \"".__DIR__.'\\'.$tempFolder."\" \"Intersect Server.exe\" \t\n";
    $console = popen('"runServer.exe" "'.__DIR__.'\\'.$tempFolder.'" "Intersect Server.exe"', 'r');
    sleep(10);
    pclose($console);

    echo "[SUCCESS]Intersect Server Initialise\t\n";
    configPanel();

}

function localServerIntersect(){
    global $folderGame;
    $folderGame = './';
    baseConfig();
    configPanel();
}

function remoteServerIntersect(){
    global $handle;
    baseConfig(false);

    $arrBD = array();
    $baseName = "gamedata";
    echo "[WARNING] Remote Server is available only MySQL !\t\n";
    do{
        $server = "localhost"; $port = "3306"; $user = "root"; $password  = ""; $databaseName = "";

        echo 'Configuration database for: '.$baseName. "\t\nServer link [".$server.']:';
        if(($value= trim(fgets($handle))) != "") $server = $value;

        echo 'Port ['.$port.']:';
        if(($value= trim(fgets($handle))) != "") $port = $value;

        echo 'User ['.$user.']:';
        if(($value = trim(fgets($handle))) != "") $user = $value;

        echo 'Password ['.$password.']:';
        if(($value= trim(fgets($handle))) != "") $password = $value;

        if($baseName == "playerdata"){
            $arrBD['PlayerDatabase'] = array(
                "Type" => "mysql",
                "Server" => $server,
                "Port" => $port,
                "Database" => $databaseName,
                "Username" => $user,
                "Password" => $password
            );
            break;
        }else{
            $arrBD['GameDatabase'] = array(
                "Type" => "mysql",
                "Server" => $server,
                "Port" => $port,
                "Database" => $databaseName,
                "Username" => $user,
                "Password" => $password
            );
            echo 'Same configuration for PlayerDatabase? [Y/n]';
            if(($value=strtolower(trim(fgets($handle)))) == 'n'){
              $baseName = "playerdata";
            }else{
                $arrBD['PlayerDatabase'] = $arrBD['GameDatabase'];
                break;
            }
        }
    }while($baseName != "gamedata");
    $config['servers']['id_0']['config_server'] = $arrBD;
    configPanel();
}

function configPanel(){
    global $handle, $titlePanel, $pathPanel, $config, $gitInstall;
    echo 'Name panel ['.$titlePanel.']:';
    if(($value = trim(fgets($handle))) != "") $titlePanel = $value;
    echo 'Path panel ['.$pathPanel.']:';
    if(($value = trim(fgets($handle))) != "") $pathPanel = $value;
    if(substr($pathPanel, 0,1) != "/") $pathPanel = '/'.$pathPanel;

    $config['website']['title'] = $titlePanel;
    $config['website']['path'] = $pathPanel;


    $dataZip = file_get_contents($gitInstall."install/Intersect_Panel.zip");
    file_put_contents('install.zip', $dataZip);
    $zip = new ZipArchive;
    $zip->open('install.zip');
    $zip->extractTo('./');
    $zip->close();
    unlink('install.zip');

    saveConfig($config, 'config.json');
}

function saveConfig($json, $file){
    if(file_exists($file)){
        $fp = fopen($file, "r+");
        ftruncate($fp, 0);
        fclose($fp);
    }
    file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
    echo "Configuration saved.\t\n";
}