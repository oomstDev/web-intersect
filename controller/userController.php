<?php


class userController extends Controller
{
    public function __construct()
    {
        parent::__construct('IsUser' );
    }

    public function indexAction()
    {
        return $this->redirect('user/account');
    }

    function logoutAction(){
        session_destroy();
        return $this->redirect('index');
    }

    function accountAction(){
        return $this->render('user/account');
    }

    function characterAction(){

        if(Router::get('id') != null)
            $character = $this->getUser()->getCharacterId($_GET['id']);
        else
            $character = $this->getUser()->getCharacter()[0];

        App::addVar('character', $character);
        return $this->render('user/character');
    }

    function removeCharacterAction(){

        if(Router::get('id') != null){
            if($this->getUser()->deleteCharacterId(Router::get('id'))){
                App::addAlert('success', 'Delete done !');
                return $this->redirect('account');
            }else{
                $error = "Error Delete Character";
            }
        }else{
            $error = "Error Delete Character : Missing ID";
        }
        App::addAlert('error', $error);

        if(Router::get('id') != null)
            return $this->redirect('user/character', array('id', Router::get('id')));
        else
            return $this->redirect('user/account');
    }

    function changePasswordAction(){
        if(Router::post('password') != null){
            $this->getUser()->setPassword(strtoupper(hash('sha256', strtoupper(hash('sha256', Router::post('password'))).$this->getUser()->getSalt())));
            App::addAlert('success', 'Password changed');
        }

        if(Router::post('email')){
            if(Check::mail(Router::post('email')) && count(Database::getRows('users', array('Email='=>Router::post('email')))) == 0){
                $this->getUser()->setMail(Router::post('email'));
                App::addAlert('success', 'Email changed');
            }
            else
                App::addAlert('error', 'Error in email');
        }
        return $this->redirect('user/account');
    }
}