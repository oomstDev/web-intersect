<?php


class serverController extends Controller
{
    public function __construct()
    {
        parent::__construct('IsModerator' );
    }

    function indexAction()
    {
        return $this->redirect('index');
    }

    public function installAction(){
        $pathRoot = Server::intersectValue('', 'intersect_folder_server');
        if(Server::intersectValue('', 'nameRoot') == ''){
            Server::intersect()['nameRoot'] = 'server'.md5(uniqid()).'.exe';
            copy($pathRoot.'/Intersect Server.exe', $pathRoot.'/'.Server::intersectValue('Intersect Server.exe', 'nameRoot'));
            Server::saveIntersect();
        }
        return $this->redirect('admin/index');
    }

    public function startAction(){
        $path = Server::intersectValue('', 'intersect_folder_server');
        $tempFolder = (substr($path, -1) == '/') ? substr($path, 0, -1) : $path;
        $tempFolder = str_replace('/', '\\', $tempFolder);

        $terminal = popen('"'.Router::dir().'/runServer.exe" "'.$tempFolder.'" "'.Server::intersectValue('Intersect Server.exe', 'nameRoot').'"', 'r');
        sleep(10);
        pclose($terminal);
        return $this->redirect('admin/index');
    }

    public function stopAction(){
        $str = exec('netstat -a -o -n -p UDP | findstr /i '.Server::intersectValue(5400, 'config_server', 'ServerPort'));
        $pid = "";
        if($str != ""){
            $letter = false;
            for($index = strlen($str); $index >= 0; $index--){
                if($letter){
                    $pid = $str[$index+1].$pid;
                    if($str[$index] == ' '){
                        break;
                    }
                }else{
                    if(isset($str[$index]) && $str[$index] != ' '){
                        $letter = true;
                    }
                }
            }
            exec('taskkill /PID '.$pid);
            sleep(5);
        }

        return $this->redirect('admin/index');
    }

    public function restartAction(){

    }
}