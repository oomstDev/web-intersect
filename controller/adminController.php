<?php

use Linfo\Linfo;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 30/03/2019
 * Time: 15:03
 */

class adminController extends Controller
{
    public function __construct()
    {
        parent::__construct('IsModerator' );
    }

    public function indexAction(){
        if(Router::websiteValue(false, 'composer', 'linfo')){
            try{
                $linfo = new Linfo;
                $parser = $linfo->getParser();
                App::addVar('linfo', $parser);
            }catch (Exception $e){
                App::addAlert('error', $e->getMessage());
            }
        }
        return $this->render('admin/index');
    }

    public function usersAction(){
        App::addVar('users', User::all(Router::get('page', 0)));
        return $this->render('admin/users');
    }

    public function mapsAction(){
        App::addVar('maps', Map::all(Router::get('page', 0)));
        return $this->render('admin/maps');
    }

    public function bansAction(){
        App::addVar('bans', Ban::all(Router::get('page', 0)));
        return $this->render('admin/bans');
    }

    public function itemsAction(){
        App::addVar('items', Item::all(Router::get('page', 0)));
        return $this->render('admin/items');
    }
    public function npcsAction(){
        App::addVar('npcs', Npc::all(Router::get('page', 0)));
        return $this->render('admin/npcs');
    }

    public function configAction(){
        $nameArr = Router::get('array', '');
        $arr = preg_split('/!/', $nameArr);
        if($arr[0] == '')
            $arr = [];

        $a = Router::getConfigJSON([], $arr);
        App::addVar('config', $a);
        App::addVar('nameArr', $nameArr);

        return $this->render('admin/config');
    }
    public function configSaveAction(){
        $nameArr = Router::post('array', '');
        $arr = preg_split('/!/', $nameArr);
        if($arr[0] == "")
            $arr = [];

        $ori = Router::getConfigJSON([]);
        $a = Router::getValueArray($ori, [], $arr);
        foreach(Router::post('config', []) as $key => $value){
            if($value=="NULL")
                unset($a[$key]);
            else
                $a[$key] = $value;
        }
        $ori = Router::setValueArray($ori, $a, $arr);
        Router::saveConfigJSON($ori);
        return $this->redirect('admin/config', array('array'=>$nameArr));
    }
}