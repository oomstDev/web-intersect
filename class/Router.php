<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 29/03/2019
 * Time: 19:05
 */

class Router
{
    private $get;
    private $post;
    private $link;
    private $file;
    private $dir;

    private $website;
    private $controller;
    private $configJSON;

    /** @var Router */
    private static $instance;
    /**
     * Router constructor.
     * @param $get
     * @param $post
     * @param $link
     * @param $file
     */
    private function __construct($get, $post, $link, $file)
    {
        $this->get = $get;
        $this->post = $post;
        $this->link = $link;
        $this->file = $file;
    }

    public static function init($path){
        $router = new Router(array(), $_POST, array(), $_FILES);
        $router->dir = $path;
        try {
            $config = $router->initYaml('config.json');
            $indexServer = 0;
            $configJSON = $config;

            foreach(self::getValueArray($config, array(), 'servers') as $key => $server){
                if(($folder = self::getValueArray($server, '', 'intersect_folder_server')) !== ''){
                    $server['config_server'] = $router->initYaml($folder.'/resources/config.json');
                    self::setValueArray($configJSON, $server['config_server'], 'servers', $key, 'config_server');
                }

                if($key == self::getValueArray($_SESSION, null, 'server'))
                    Server::intersect($server);

                Server::addServerEnable(array('name' => self::getValueArray($server, 'Server'.$indexServer, 'name'), 'id'=>$key));

                $indexServer++;
            }
            if(Server::intersect() === null){
                $arr = $configJSON['servers'];
                reset($arr);
                Server::intersect(current($arr));
            }
            $router->website = self::getValueArray($config, array(), 'website');
            $router->configJSON = $configJSON;

            if(!is_dir($router->dir.'/vendor'))
                $router->website['vendor'] = array();

        } catch (Exception $e) {
            die($e->getMessage());
        }
        self::$instance = $router;

    }

    public static function getValueArray(&$array, $defaultValue, ...$keySearch){
        if($array == null)return $defaultValue;
        if(is_array($keySearch[0])) $keySearch = $keySearch[0];
        if(count($keySearch) == 0) return $array;

        if(!key_exists($keySearch[0], $array)) return $defaultValue;

        if(count($keySearch) == 1){
            foreach($array as $key => $value)
                if($keySearch[0] == $key){
                    return $value;
                }
        }
        if(is_array($array[$keySearch[0]])){
            return self::getValueArray($array[$keySearch[0]], $defaultValue, array_slice($keySearch, 1));
        }
        return $defaultValue;
    }

    public static function setValueArray(&$array, $newValue, ...$keySearch){
        if(count($keySearch) > 0 && is_array($keySearch[0])) $keySearch = $keySearch[0];
        $k = reset($keySearch);
        if(count($keySearch) == 0){
            $array = $newValue;
            return $newValue;
        }
        if(count($keySearch) == 1){
            $array[$k] = $newValue;
            return $array;
        }
        foreach($array as $key => $value){
            if(is_array($array[$k])){
                $array[$k] = self::setValueArray($array[$k], $newValue, array_slice($keySearch, 1));
            }
        }
        return $array;
    }

    public static function initLink($url){
        $index = strpos($url, '?');
        $ga = array();
        $link = $url;
        if($index !== false){
            $g = substr($url, $index+1);
            $link = substr($url, 0, $index);
            $index = strpos($g, '&');
            $ge = array($g);
            if($index !== false){
                $ge = preg_split("/&/", $g);
            }
            foreach($ge as $value){
                $v = preg_split("/=/", $value);
                $ga[$v[0]] = $v[1];
            }
        }
        self::$instance->get = $ga;
        if(substr($link, -1) == "/")$link=substr($link,0, -1);
        if(substr($link, 0,1) == "/")$link=substr($link,1);

        $index = strpos($link, '/');
        if($index !== false){
            self::$instance->controller = substr($link, 0, $index);
            $link = substr($link, $index+1);
        }else
            self::$instance->controller = "index";

        self::$instance->link = $link;
    }

    /**
     * @param $str
     * @return array
     * @throws Exception
     */
    private function initYaml($str){
        if(!is_file($str))
            throw new Exception("Missing config: ".$str);
        return json_decode(file_get_contents($str), true);
    }

    /**
     * @return array
     */
    private static function &website(){
        return self::$instance->website;
    }
    public static function websiteValue($defaultValue, ... $arr){
        if(count($arr) > 0 && is_array($arr[0])) $arr = $arr[0];
        return self::getValueArray(self::website(), $defaultValue, $arr);
    }

    /**
     * @param string $k
     * @param string $defaultValue
     * @return mixed
     */
    public static function get($k, $defaultValue = null)
    {
         foreach(self::$instance->get as $key => $value){
             if($key == $k)
                 return $value;
         }

         return $defaultValue;
    }


    /**
     * @param string $k
     * @param string $defaultValue
     * @return mixed
     */
    public static function post($k, $defaultValue = "")
    {
         foreach(self::$instance->post as $key => $value)
             if($key == $k)
                 return $value;
         return $defaultValue;
    }

    public static function file(){
        return self::$instance->file;
    }

    public static function link(){
        return self::$instance->link;
    }

    public static function controller(){
        return self::$instance->controller;
    }

    public static function URL(){
        return self::controller().'/'.self::link();
    }

    public static function &getConfigJSON($defaultValue, ... $arr){
        if(count($arr) > 0 && is_array($arr[0])) $arr = $arr[0];
        return self::getValueArray(self::$instance->configJSON, $defaultValue, $arr);
    }

    public static function getConfigJSONRef($defaultValue, ... $arr){
        if(count($arr) > 0 && is_array($arr[0])) $arr = $arr[0];
        return self::getValueArray(self::$instance->configJSON, $defaultValue, $arr);
    }

    public static function saveConfigJSON($ori){
        $fp = fopen("config.json", "r+");
        ftruncate($fp, 0);
        fclose($fp);
        file_put_contents('config.json', json_encode($ori, JSON_PRETTY_PRINT));
    }

    public static function dir(){
        return self::$instance->dir;
    }
}