<?php


class Shops extends Table
{
    protected $id;
    public $timeCreated;
    public $name;
    public $buyingWhitelist;
    public $defaultCurrency;
    public $buyingItems;
    public $sellingItems;

    function init()
    {
        // TODO: Implement init() method.
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @param mixed $timeCreated
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBuyingWhitelist()
    {
        return $this->buyingWhitelist;
    }

    /**
     * @param mixed $buyingWhitelist
     */
    public function setBuyingWhitelist($buyingWhitelist)
    {
        $this->buyingWhitelist = $buyingWhitelist;
    }

    /**
     * @return mixed
     */
    public function getDefaultCurrency()
    {
        return $this->defaultCurrency;
    }

    /**
     * @param mixed $defaultCurrency
     */
    public function setDefaultCurrency($defaultCurrency)
    {
        $this->defaultCurrency = $defaultCurrency;
    }

    /**
     * @return mixed
     */
    public function getBuyingItems()
    {
        return $this->buyingItems;
    }

    /**
     * @param mixed $buyingItems
     */
    public function setBuyingItems($buyingItems)
    {
        $this->buyingItems = $buyingItems;
    }

    /**
     * @return mixed
     */
    public function getSellingItems()
    {
        return $this->sellingItems;
    }

    /**
     * @param mixed $sellingItems
     */
    public function setSellingItems($sellingItems)
    {
        $this->sellingItems = $sellingItems;
    }
}