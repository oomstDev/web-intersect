<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 28/03/2019
 * Time: 17:17
 */

class Database
{
    private $type, $link, $user, $password, $databaseName, $charset, $port;
    private static $name, $defaultName;
    private static $dbs = array();
    public static $nbrSQL = 0, $nbrCache = 0;

    /**
     * Database constructor.
     * @param $type
     * @param $link
     * @param $user
     * @param $password
     * @param $databaseName
     * @param $charset
     * @param $port
     */
    private function __construct($type, $link, $user, $password, $databaseName, $charset, $port)
    {
        $this->type = $type;
        $this->link = $link;
        $this->user = $user;
        $this->password = $password;
        $this->databaseName = $databaseName;
        $this->charset = $charset;
        $this->port = $port;
    }

    public static function clean(){
        self::$dbs = array();
    }


    public static function addDB($name, $link, $user = '', $password = '', $port = 3306, $databaseName = '', $type = 'mysql', $charset = 'utf8')
    {
        if(strtolower($type) == 'sqlite'){
            if(!is_file($link)){
                var_dump("[".$type."] Missing File: ".$link);
                die();
            }
        }

        $s = new Database($type, $link, $user, $password, $databaseName, $charset, $port);
        self::$dbs[$name] = $s;
    }

    /**
     * @return Database
     */
    private static function getInstance(){
        return self::$dbs[self::$name];
    }
    public static function setDefaultName($name){
        if(self::$defaultName != null) return;

        self::$defaultName = $name;
        self::$name = $name;
    }
    public static function setDB($name){
        self::$name = $name;
    }

    public static function getRows($from, $where = array(), $orderBy = "", $limit = "", $blob = array()){
        $result = array();
        /** @var PDO $connection */
        $connection = self::getConnection();
        if($connection != null){
            if(Database::getInstance()->type == 'sqlite'){
                $whereLink = $where;
                $ex = "";
                foreach($where as $key => $w){
                    $v = null;
                    if(substr($key, -1) == "="){
                        $v = substr($key, 0, -1);
                        $ex = "=";
                    }
                    if(substr($key, -5) == " LIKE"){
                        $v = substr($key, 0, -5);
                        $ex = " LIKE";
                    }
                    $whereLink[$v] = $key;
                }

                $sql = 'SELECT';
                foreach ($blob as $b){
                    $sql .= " LOWER(SUBSTR(HEX($b), 7, 2)||SUBSTR(HEX($b), 5, 2)||SUBSTR(HEX($b), 3, 2)||SUBSTR(HEX($b), 1, 2) ||'-'||SUBSTR(HEX($b), 11, 2)||SUBSTR(HEX($b), 9, 2) ||'-'||SUBSTR(HEX($b), 17, 4) ||'-'|| SUBSTR(HEX($b), 21)) as bl$b,";

                    if(key_exists($b,$whereLink)){
                        $v = $where[$whereLink[$b]];
                        if(strlen($v) > 32)
                            $v = substr($v, 0, 14).substr($v, 19);

                        $where['bl'.$b.$ex] = $v;
                        unset($where[$whereLink[$b]]);
                    }
                }
                $sql .= ' * FROM '.$from;
            }
            else
                $sql = 'SELECT * FROM '.$from;

            $para = array();
            if(count($where) > 0){
                $sql .= ' WHERE';
                foreach($where as $key => $value){
                    $sql .= ' '.$key.' ? AND';
                    $para[] = $value;
                }
                $sql = substr($sql, 0, -4);
            }
            if($orderBy != "")
                $sql .= " ORDER BY ".$orderBy;

            if($limit != "")
                $sql .= " LIMIT ".$limit;

            $result = self::actionBDD($sql, $para, $blob);
        }
        return $result;
    }

    public static function getRow($from, $where = array(), $orderBy = "", $limit = "", $blob = array()){
        $row =  Database::getRows($from, $where, $orderBy, $limit, $blob);
        if(count($row) == 0) return null;
        else return $row[0];
    }
    //TODO A REWORK !!
    public static function updateRow($from, $where, $condition){
        $result = null;
        if(count($condition) == 0 || count($where) == 0) return $result;

        $connection = self::getConnection();
        if($connection != null){
            $para = array();
            $sql = 'UPDATE '.$from.' SET';
            $index = 1; $max = count($condition);
            foreach ($condition as $key => $value){
                $sql .= ' '.$key.'= ?';
                $para[] = $value;
                if($index < $max)
                    $sql .= ',';
                $index++;
            }
            $index = 1; $max = count($where);
            $sql .= ' WHERE';
            foreach ($where as $key => $value){
                $sql .= ' '.$key.'= ?';
                $para[] = $value;
                if($index < $max)
                    $sql .= ' AND';
                $index++;
            }
            $result = self::actionBDD($sql, $para);
        }
        return $result;
    }

    //TODO A REWORK !!
    public static function deleteRow($from, $where){
        $result = null;
        if(count($where) == 0) return $result;

        $connection = self::getConnection();
        if($connection != null){
            $para = array();
            $sql = 'DELETE FROM '.$from .' WHERE';
            $index = 1; $max = count($where);
            foreach ($where as $key => $value){
                $sql .= ' '.$key.'= ?';
                $para[] = $value;
                if($index < $max)
                    $sql .= ' AND';
                $index++;
            }
            self::actionBDD($sql, $para);
            return true;
        }
        return false;
    }

    /**
     * @param string $sql
     * @param array $where
     * @param array $blob
     * @return array
     */
    public static function actionBDD($sql, $where = array(), $blob = array()){
        $cacheDB = self::getCache($sql, $where);
        if($cacheDB !== null) {
            self::$nbrCache++;
            self::$name = self::$defaultName;
            return $cacheDB;
        }
        $statement = self::getConnection()->prepare($sql);
        $re = [];
        if($statement){
            $statement->execute($where);
            while($row = $statement->fetch(PDO::FETCH_ASSOC)){
                $r = $row;
                if(self::getInstance()->type == 'sqlite'){
                    foreach ($blob as $b){
                        if(key_exists('bl'.$b, $r)){
                            $r[$b] = $row['bl'.$b];
                            unset($r['bl'.$b]);
                        }
                    }
                }
                $re[] = $r;
            }
            $statement->closeCursor();
        }
        self::$nbrSQL++;
        self::$name = self::$defaultName;
        self::setCache($sql, $where, $re);
        return $re;
    }

    /**
     * @return PDO|null
     */
    private static function getConnection(){
        $i = self::getInstance();
        switch($i->type){
            case 'mysql':
                return $connection = $i->mySQL();
                break;
            case 'sqlite':
                return $connection = $i->sqlLite();
                break;
            default:
                return null;
        }
    }
    /**
     * @return PDO
     */
    private function mySQL(){
        $i = self::getInstance();
        try{
            return new PDO('mysql:host='.$i->link.':'.$i->port.';dbname='.$i->databaseName.';charset='.$i->charset, $i->user, $i->password);
        }catch(Exception $e){
            var_dump($e);
            die();
        }
    }

    /**
     * @return PDO
     */
    private function sqlLite(){
        $i = self::getInstance();
        try{
            return new PDO('sqlite:'.$i->link);
        }catch (Exception $e){
            var_dump($e);
            die();
        }
    }

    private static function getCache($select, $where){
        foreach($where as $k => $v){
            $select.= $k.'+'.$v;
        }
        return App::getCache('db/', $select);
    }

    private static function setCache($select, $where, $row){
        foreach($where as $k=>$v){
            $select.= $k.'+'.$v;
        }
        App::setCache('db/', $select, $row);
    }
}