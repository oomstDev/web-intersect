<?php


class PlayerVariable extends Table
{
    protected $id;
    protected $timeCreated;
    protected $hidden;
    public $name;
    public $textId;
    public $raw;

    public function __construct()
    {
        parent::__construct( array('Id') );
    }

    function init()
    {
        $row = Server::intersectValue(null,'playerVariables', $this->textId);
        $hidden = true;
        if($row != null){
            if(!empty($nameI = Router::getValueArray($row, null, 'name')))
                $this->name = $nameI;
            $hidden = Router::getValueArray($row, true, 'hidden');
        }
        $this->hidden = $hidden;
        $this->raw = array(
            'Name'=>$this->name,
            'TextId'=>$this->textId
        );
    }

    public static function getVariableTextId($id){
        Database::setDB("GameDatabase");
        return self::createClass(Database::getRow('PlayerVariables', array('TextId='=>$id)), PlayerVariable::class);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTextId()
    {
        return $this->textId;
    }

    /**
     * @return bool
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

}