<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 01/04/2019
 * Time: 19:53
 */

class Item extends Table
{
    protected $id;
    public $name;


    public static function all($page = 0, $step = 25){
        $a = array();
        Database::setDB("GameDatabase");
        foreach(Database::getRows('Items', array(), '', $page*$step.''.$step) as $row){
            $a[] = self::createClass($row, Item::class);
        }
        return $a;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    function init()
    {
        // TODO: Implement init() method.
    }
}